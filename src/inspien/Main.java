package inspien;



import java.util.List;

import org.jsoup.nodes.Document;

import beans.Movies;
import beans.Movies.Movie;
import parser.ElasticSearch;
import parser.HTMLParser;
import parser.XMLParser;

public class Main {
	
	public static void main(String[] args){
		
		// 1. parsingClass 선언
		HTMLParser htmlParser = new HTMLParser();
		XMLParser xmlParser = new XMLParser();
		ElasticSearch elastic = new ElasticSearch();
		
		//2. URL로부터 HTML 요청
		Document doc = htmlParser.parseFromUrl("https://namu.wiki/w/%ED%95%9C%EA%B5%AD%20%EC%98%81%ED%99%94/%EB%AA%A9%EB%A1%9D");
		
		//3. 도크먼트로부터 영화목록 파싱하여 VO에 저장
		Movies movies = htmlParser.selectAll(doc);
		
		//4. VO를 XML로 변환
		xmlParser.parsingXML(movies);
		
		//5. XML을 Elastic에 저장
		xmlParser.submitToElastic("result.xml");
		
	
		
		
	}

}





