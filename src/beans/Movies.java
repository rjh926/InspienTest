
package beans;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "movie" })
@XmlRootElement(name = "movies")
public class Movies {

	private List<Movies.Movie> movie;

	//SingletonPattern
	public List<Movies.Movie> getMovie() {
		if (movie == null) {
			movie = new ArrayList<Movies.Movie>();
		}
		return this.movie;
	}

	public void setMovie(List<Movies.Movie> movie) {
		this.movie = movie;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "Movie", propOrder = { "value" })
	public static class Movie {

		@XmlValue
		protected String value;
		
		@XmlAttribute(name = "link")
		protected String link;

		//Setter, Getter
		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getLink() {
			return link;
		}

		public void setLink(String value) {
			this.link = value;
		}

	}

}
