

package beans;

import javax.xml.bind.annotation.XmlRegistry;


@XmlRegistry
public class ObjectFactory {


    
    public ObjectFactory() {
    }

  
    public Movies createMovies() {
        return new Movies();
    }

    public Movies.Movie createMoviesMovie() {
        return new Movies.Movie();
    }

}
