package parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import beans.Movies;
import beans.Movies.Movie;

public class XMLParser {
	

	// VO를 XML로 Converting
	public void parsingXML(Movies movies) {
		try {

			// XML을 저장할 File 선언
			File file = new File("result.xml");

			JAXBContext context = JAXBContext.newInstance(Movies.class);
			Marshaller marshaller = context.createMarshaller();

			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(movies, System.out);
			marshaller.marshal(movies, file);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void submitToElastic(String fileName) {
		ElasticSearch elastic = new ElasticSearch();
		try {
			File file = new File(fileName);
			JAXBContext context = JAXBContext.newInstance(Movies.class);
			Unmarshaller um = context.createUnmarshaller();

			Movies movies = (Movies) um.unmarshal(file);
			int count =0;
			for (Movie m : movies.getMovie()) {
				JSONObject obj = new JSONObject();
				obj.put("link",m.getLink());
				obj.put("value",m.getValue());
				elastic.indexing(obj.toString(),Integer.toString(count++));
				
			}
			

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();			
		}
	}
	
	public List<Movie> getMovies(String fileName){
		List<Movie> result = new ArrayList<>();
		File file = new File(fileName);
		try {
			
			JAXBContext context= JAXBContext.newInstance(Movies.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			
			Movies movies = (Movies) unmarshaller.unmarshal(file);
			
			for(Movie m : movies.getMovie()) {
				result.add(m);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	

}
