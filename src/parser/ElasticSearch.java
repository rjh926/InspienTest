package parser;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.transport.client.PreBuiltTransportClient;


public class ElasticSearch {
	Settings settings;
	Client client;
	
public ElasticSearch() {
	
	settings = Settings.builder()
			.put("cluster.name","my-application").build();
	try {
		client = new PreBuiltTransportClient(settings).addTransportAddress(new TransportAddress(InetAddress.getByName("127.0.0.1"),9300));
	} catch (UnknownHostException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}

public void indexing(String json,String id) {
	IndexResponse response = client.prepareIndex("movies","movie",id).setSource(json,XContentType.JSON).get();

}
	

	
	

}
