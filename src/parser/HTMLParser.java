package parser;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import beans.Movies;
import beans.Movies.Movie;

public class HTMLParser {

	// URL로부터 HTML 추출
	public Document parseFromUrl(String url) {
		Document doc = null;

		try {
			doc = Jsoup.parse(new URL(url), 1000);

	} catch (IOException e) {
			e.printStackTrace();
	}
		return doc;
	}

	// HTML 도큐먼트로부터 영화목록을 추출하여 VO에 저장
	public Movies selectAll(Document doc) {
		Movies movies = new Movies();
		Elements elements = doc.select(".wiki-link-internal");
		List<Movie> list = movies.getMovie();
		
		for (Element e : elements) {
			Movie movie = new Movie();
			movie.setLink(e.attr("href"));
			movie.setValue(e.text());

			list.add(movie);
		}
		movies.setMovie(list);
		return movies;
	}

}
